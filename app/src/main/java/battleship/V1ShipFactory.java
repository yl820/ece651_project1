package battleship;

public class V1ShipFactory implements AbstractShipFactory<Character>{

    /**
   * create rectangleship 
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created
   */
    protected Ship<Character> createRectangleShip(Placement where, int w, int h, char letter, String name) {
        char style_upper = Character.toUpperCase(where.getOrientation());
        if(style_upper != 'V' && style_upper != 'H'){
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format. ");
        }
        if(where.getOrientation() == 'v' || where.getOrientation() == 'V' ){
            return new RectangleShip<Character>(name, where.getWhere(), w, h, letter,'*');
        }
        return new RectangleShip<Character>(name, where.getWhere(), h, w, letter,'*');
    }

    /**
   * create non-rectangleship 
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the ship created
   */ 
    protected Ship<Character> createNonRectangleShip(Placement where, char letter, String name) {
        return new NonRectangleShip<Character>(name, where.getOrientation(), where.getWhere(), letter,'*');

    }

    /**
   * Make a submarine.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the submarine.
   */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createRectangleShip(where, 1, 2, 's', "Submarine");
    }

    /**
   * Make a battleship.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the battleship.
   */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createRectangleShip(where, 1, 4, 'b', "Battleship");
    }

    /**
   * Make a carrier.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the carrier.
   */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createRectangleShip(where, 1, 6, 'c', "Carrier");
    }

    /**
   * Make a rectangleship destroyer.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the destroyer.
   */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createRectangleShip(where, 1, 3, 'd', "Destroyer");
    }

    /**
   * Make a non-rectangleship carrier.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the carrier.
   */
    @Override
    public Ship<Character> makeCarrier_new(Placement where) {
        return createNonRectangleShip(where, 'c', "Carrier");
    }

    /**
   * Make a non-rectangleship destroyer.
   * 
   * @param where specifies the location and orientation of the ship to make
   * @return the Ship created for the destroyer.
   */
    @Override
    public Ship<Character> makeBattleship_new(Placement where) {
        return createNonRectangleShip(where, 'b', "Battleship");
    }
}
