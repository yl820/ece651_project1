package battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
    
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
   * check if all coordinate of a ship in in bound
   * 
   * @param theShip to check 
   * @param theBoard whose theShip has to be checked
   * @return the error description (null if there is no error)
   */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        Iterable<Coordinate>  coordinate_list = theShip.getCoordinates();
        for (Coordinate c : coordinate_list){
            int row = c.getRow();
            int column = c.getColumn();
            if (row < 0){
                return "That placement is invalid: the ship goes off the top of the board.";
            }else if(row >= theBoard.getHeight()){
                return "That placement is invalid: the ship goes off the bottom of the board.";
            }else if(column < 0 ){
                return "That placement is invalid: the ship goes off the left of the board.";
            }else if(column  >=  theBoard.getWidth()){
                return "That placement is invalid: the ship goes off the right of the board.";
            }
        }
        return null;
    }
}
