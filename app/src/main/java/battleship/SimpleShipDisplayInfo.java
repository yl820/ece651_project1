package battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    // myData is the real char
    T myData;
    // onHit is what will be displayed on the players own board as hitted
    T onHit;

    /**
   * initialize the SimpleShipDisplayInfo
   */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
   * get the info we want at the coordinate where
   * 
   * @param where is the coordinate to return information for
   * @param hit is the coordinate to return information for
   * @return The view-specific information at that coordinate.
   */
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        } 
        return myData;
    }
}
