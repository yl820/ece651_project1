package battleship;

import java.util.ArrayList;

public class NonRectangleShip<T> extends BasicShip<T>{
    // the name of the ship
    private final String name;
    // the style of the ship
    private final char  style;


    /**
   * build the non-Rectangle ships in given order
   * 
   * @param upperLeft to check 
   * @param name whose theShip has to be checked
   * @param style the style the ship we want ato add 
   * @return the array list pf coordinates
   */
    static ArrayList<Coordinate> makeCoords_NonRectangle(Coordinate upperLeft,String name,char style){
        ArrayList<Coordinate> ans = new ArrayList<Coordinate>();
        char style_upper = Character.toUpperCase(style);
        int curr_row = upperLeft.getRow();;
        int curr_column = upperLeft.getColumn();
        // check the style index is in the correct format
        if(style_upper != 'U' && style_upper != 'R' && style_upper != 'B' && style_upper != 'L' ){
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        // build the ships in the given order to make sure the hitted coordinate can be record when the style of ship change
        if( name == "Battleships"){
            if(style_upper == 'U'){
                ans.add(new Coordinate(curr_row,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column+2));
            }else if(style_upper == 'R'){
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row,curr_column));
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row+2,curr_column));
            }else if(style_upper == 'D'){
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row,curr_column+2));
                ans.add(new Coordinate(curr_row,curr_column+1));
                ans.add(new Coordinate(curr_row,curr_column));
            }else{
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row+2,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row,curr_column+1));
            }
        }else{
            if(style_upper == 'U'){
                ans.add(new Coordinate(curr_row,curr_column));
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row+2,curr_column));
                ans.add(new Coordinate(curr_row+3,curr_column));
                ans.add(new Coordinate(curr_row+2,curr_column+1));
                ans.add(new Coordinate(curr_row+3,curr_column+1));
                ans.add(new Coordinate(curr_row+4,curr_column+1));
            }else if(style_upper == 'R'){
                ans.add(new Coordinate(curr_row,curr_column+4));
                ans.add(new Coordinate(curr_row,curr_column+3));
                ans.add(new Coordinate(curr_row,curr_column+2));
                ans.add(new Coordinate(curr_row,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column+2));
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column));
            }else if(style_upper == 'D'){
                ans.add(new Coordinate(curr_row+4,curr_column+1));
                ans.add(new Coordinate(curr_row+3,curr_column+1));
                ans.add(new Coordinate(curr_row+2,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row+2,curr_column));
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row,curr_column));
            }else{
                ans.add(new Coordinate(curr_row+1,curr_column));
                ans.add(new Coordinate(curr_row+1,curr_column+1));
                ans.add(new Coordinate(curr_row+1,curr_column+2));
                ans.add(new Coordinate(curr_row+1,curr_column+3));
                ans.add(new Coordinate(curr_row,curr_column+2));
                ans.add(new Coordinate(curr_row,curr_column+3));
                ans.add(new Coordinate(curr_row,curr_column+4));
            }
        }
        return ans;
    }

    /**
   * get the name of ship
   * 
   * @return the name of ship
   */
    public String getName(){
        return name;
    }

    /**
   * get the style of ship
   * 
   * @return the style of ship
   */
    public char getStyle(){
        return style;
    }

    /**
   * initialize the non-RectangleShip
   */
    public NonRectangleShip(String name,char style, Coordinate upperLeft,ShipDisplayInfo<T> myDisplayInfo,ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords_NonRectangle(upperLeft, name, style), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
        this.style = style;
    }

    /**
   * initialize the non-RectangleShip
   */
    public NonRectangleShip(String name,char style,Coordinate upperLeft, T data, T onHit) {
        this(name,style,upperLeft,new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data));
    }
    
    /**
   * initialize the non-RectangleShip
   */
    public NonRectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship",'U', upperLeft, data, onHit);
    }
}