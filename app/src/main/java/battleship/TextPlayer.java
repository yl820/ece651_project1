package battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

public class TextPlayer {
    private final Board<Character> theBoard;
    private final BoardTextView view;
    private final BufferedReader inputReader;
    private final PrintStream out;
    private final AbstractShipFactory<Character> shipFactory;
    private final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    // special action remain for player
    private HashMap<Character,Integer> action_remain;
    // if the player is a computer 
    public boolean IsComputer;

    public BoardTextView getView(){
        return view;
    }

    public String getName(){
        return name;
    }

    public Board<Character> getBoard(){
        return theBoard;
    }

    /**
   * create all the ships needed in game
   */
    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeSubmarine(p));
    }

    /**
   * record all the ship have to be added in the game
   */
    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
    }

    public TextPlayer(String name,Board<Character> theBoard,BufferedReader inputReader,PrintStream out,AbstractShipFactory<Character> shipFactory){
        this.inputReader = inputReader;
        this.name = name;
        this.out = out;
        this.shipFactory = shipFactory;
        this.theBoard= theBoard;
        this.view = new BoardTextView(theBoard);
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        // set the remain action oppotunities to 3 
        this.action_remain = new HashMap<Character,Integer>(){
            {
            put('M', 3);  
            put('S', 3);
        }
        };
        this.IsComputer = false;
    }

    /**
   * read a Placement from inputReader
   * 
   * @param prompt the information we want to show before user input there message
   * @return the Placement we get 
   */
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        out.print("---------------------------------------------------------------------------\n");
        String s = inputReader.readLine();
        return new Placement(s);
    }

    /**
   * read a coordiante from inputReader
   * 
   * @param prompt the information we want to show before user input there message
   * @return the coordinate we get 
   */
    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        out.print("---------------------------------------------------------------------------\n");
        String s = inputReader.readLine();
        return new Coordinate(s);
    }

    /**
   * let computer set a coordinate randomly
   * 
   * @return the coordinate computer get randomly
   */
    public Coordinate SetCoordinate(){
        Random random = new Random();
        // get a random number for row between 0 and Height()-1
        int row = random.nextInt(this.theBoard.getHeight()-1);
        // get a random number for column between 0 and Width()-1
        int column = random.nextInt(this.theBoard.getWidth()-1);
        return new Coordinate(row, column);
    }

    /**
   * let computer set a placement way randomly
   * 
   * @param nameString the name message which need to be place
   * @return the placement style/orientation computer get randomly
   */
    public Placement SetPlacement(String nameString){
        if(nameString == "Submarine"|| nameString =="Destroyer"){
            double temp = Math.random();
            // there are 2 different orientations 
            if(temp < 0.5){
                return new Placement(SetCoordinate(),'v');
            }else{
                return new Placement(SetCoordinate(),'h');
            }
        }else{
            double temp = Math.random();
            // there are 4 different styles for Carrier and battleship
            if(temp<0.25){
                return new Placement(SetCoordinate(),'u');
            }else if(temp<0.5){
                return new Placement(SetCoordinate(),'r');
            }else if(temp<0.75){
                return new Placement(SetCoordinate(),'d');
            }else{
                return new Placement(SetCoordinate(),'l');
            }
        }
    }

    /**
   * place one ship
   * 
   * @param shipName the shipname of the ship we are building
   * @param createFn the method we make one ship
   */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        while(true){
            try {
                if(!IsComputer){
                    // keep trying to place the ship until no error in placeing
                    Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                    Ship<Character> s = createFn.apply(p);
                    String ans = theBoard.tryAddShip(s);
                    // check if there are some error messages during the placement
                    if(ans == null){
                        out.print("---------------------------------------------------------------------------\n");
                        out.print(view.displayMyOwnBoard());
                        out.print("---------------------------------------------------------------------------\n");
                        return;
                    }
                    out.print("---------------------------------------------------------------------------\n");
                    out.print(ans+'\n'+"---------------------------------------------------------------------------\n");
                    out.print(view.displayMyOwnBoard());
                    out.print("---------------------------------------------------------------------------\n");
                }else{
                    // print nothing if it is computer player
                    Placement p = SetPlacement(shipName);
                    Ship<Character> s = createFn.apply(p);
                    String ans = theBoard.tryAddShip(s);
                    if(ans == null){
                        return;
                    }
                }
            } catch (IllegalArgumentException e) {
                // print the error message if it is not a computer player
                if(!IsComputer){
                    out.print("---------------------------------------------------------------------------\n");
                    out.print(e.getMessage()+"\n"+"---------------------------------------------------------------------------\n");
                }
            }
        }
    }    

    /**
   * do the placement part of game
   */
    public void doPlacementPhase() throws IOException{
        // prepare the ships list and map 
        setupShipCreationList();
        setupShipCreationMap();

        // print the explanation for players
        if(!IsComputer){
            out.print("---------------------------------------------------------------------------\n");
            out.print(view.displayMyOwnBoard());
            out.print("---------------------------------------------------------------------------\n");
            out.print("Player "+ name + " you are going to place the following ships (which are all\n");
            out.print("rectangular). For each ship, type the coordinate of the upper left\n");
            out.print("side of the ship, followed by either H (for horizontal) or V (for\n");
            out.print("vertical).  For example M4H would place a ship horizontally starting\n");
            out.print("at M4 and going to the right.  You have\n \n");
            out.print("2 \"Submarines\" ships that are 1x2\n");
            out.print("3 \"Destroyers\" that are 1x3\n");
            out.print("3 \"Battleships\" which is size 4 \n");
            out.print("2 \"Carriers\" which is size 7\n");
            out.print("---------------------------------------------------------------------------\n");
        }

        // do placement of ships
        doOnePlacement("Submarine", (p)->shipFactory.makeSubmarine(p));
        doOnePlacement("Submarine", (p)->shipFactory.makeSubmarine(p));
        doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
        doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
        doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
        doOnePlacement("Battleship", (p)->shipFactory.makeBattleship_new(p));
        doOnePlacement("Battleship", (p)->shipFactory.makeBattleship_new(p));
        doOnePlacement("Battleship", (p)->shipFactory.makeBattleship_new(p));
        doOnePlacement("Carrier", (p)->shipFactory.makeCarrier_new(p));
        doOnePlacement("Carrier", (p)->shipFactory.makeCarrier_new(p));
    }

    /**
   * ask for one coordinate input to fire at 
   * 
   * @param enemyTextPlayer the textplay object of ememy
   * @param enemyBoard the board of enemy
   * @return the coordinate we get as the coordinate to fire at
   */
    public Coordinate doOneFireInput(TextPlayer enemyTextPlayer, Board<Character> enemyBoard) throws IOException {
        while(true){
            try{
                // ask for one coordiante input
                out.print("---------------------------------------------------------------------------\n");
                Coordinate input_Coordinate = readCoordinate("player "+ this.getName() + " where do you want to fire on the "+ enemyTextPlayer.getName()+ "'s ocean?");
                int row = input_Coordinate.getRow() ; 
                int column = input_Coordinate.getColumn() ;
                out.print("---------------------------------------------------------------------------\n");
                // check if the input coordinate have the correct format
                if(row < 0 || column < 0 || row >= enemyBoard.getHeight()|| column >= enemyBoard.getWidth()){
                    throw new IllegalArgumentException("input coordinate is out of the emeny's ocean!");
                }
                return input_Coordinate;
            }catch(IllegalArgumentException e){
                out.print(e.getMessage()+"\n");
                out.print("---------------------------------------------------------------------------\n");
            }
        }
    }

    /**
   * ask for one choose of behavior
   * 
   * @return behavior player want to choose
   */
    public char DoOneChoose() throws IOException{
        // show the action player remain 
        out.print("---------------------------------------------------------------------------\n");
        out.print("Possible actions for Player "+ name +" :\n \n");
        out.print("F Fire at a square\n");
        out.print("M Move a ship to another square ("+ action_remain.get('M') +" remaining)\n");
        out.print("S Sonar scan ("+ action_remain.get('S') +" remaining)\n\n");
        out.print("Player "+ name +" what would you like to do?\n");
        out.print("---------------------------------------------------------------------------\n");
        String input = inputReader.readLine();
        char choice = input.charAt(0);
        // check the format of choose
        if(input.length() != 1 || !Character.isLetter(choice)){
            throw new IllegalArgumentException("input anction's format is wrong!");
        }
        return choice;
    }


    /**
   * let computer player to choose one action randomly
   * 
   * @return action computer player choose
   */
    public char Computer_choose(){
        double temp = Math.random();
        if(temp < 0.33){
            return 'F';
        }else if(temp < 0.66){
            return 'M';
        }else{
            return 'S';
        }
    }

    /**
   * add a new ship 
   * 
   * @param shipName the ship name to add 
   */
    public void addNewShip(String shipName) throws IOException{
        if(shipName == "Submarine"){
            doOnePlacement("Submarine", (p)->shipFactory.makeSubmarine(p));
        }else if (shipName == "Destroyer"){
            doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
        }else if (shipName == "Battleship"){
            doOnePlacement("Battleship", (p)->shipFactory.makeBattleship_new(p));
        }else{
            doOnePlacement("Carrier", (p)->shipFactory.makeCarrier_new(p));
        }
    }

    /**
   * check if a coordinate is occupied
   * 
   * @param shipName the ship name to add 
   * @return ship name if there are ship on the given coordiante
   */
    public Ship<Character> delete_checker(Coordinate c){
        for(Ship<Character> s: this.theBoard.getMyShips()){
            if(s.occupiesCoordinates(c)){
                return s;
            }
        }
        throw new IllegalArgumentException("the coordinate you input is not occupied, please choose a action again!");
    }

    /**
   * play one fire part of game
   * 
   * @param enemyTextPlayer the textplay object of ememy
   */
    public void fire_action(TextPlayer enemyTextPlayer) throws IOException{
        Board<Character> enemyBoard = enemyTextPlayer.getBoard();
        Coordinate new_Coordinate = SetCoordinate();
        if(!IsComputer){
            // get the coordiante to fire at
            new_Coordinate = doOneFireInput(enemyTextPlayer,enemyBoard);
        }
        Ship<Character> firedShip = enemyBoard.fireAt(new_Coordinate);
        // check if some ship is hitted
        if(firedShip == null){
            if(!IsComputer){
                out.print("You missed!\n");
            }else{
                out.print("player " + this.name +" miss to hit your ship!\n");
            }
        }else{
            if(IsComputer){
                out.print("player " + this.name +" hit your " + firedShip.getName() + " at " +new_Coordinate.toString()+ " !\n");
            }else{
                out.print("You hit a " + firedShip.getName() + " !\n");
            }
        }
    }

    /**
   * ask player to input if they what player to be computer 
   * 
   * @param name name of player
   */
    public void check_if_computer(String name) throws IOException{
        out.print("---------------------------------------------------------------------------\n");
        out.print("please input y/Y if you want player "+ name + " be a computer?\n");
        out.print("---------------------------------------------------------------------------\n");
        String s = inputReader.readLine();
        out.print("---------------------------------------------------------------------------\n");
        if(s.length() == 1 && (s.charAt(0) == 'Y' || s.charAt(0) == 'y')){
            this.IsComputer = true;
            out.print("you have set player "+ name +" to a computer!\n");
        }
    }

    /**
   * do the play part of the game after the placement of ships
   * 
   * @param enemyTextPlayer the textplay object of ememy
   */
    public void DoPlayPhase(TextPlayer enemyTextPlayer) throws IOException{
        String winner = "";
        while(true){
           this.playOneTurn(enemyTextPlayer);
           // check if ememy lost all the ships
            if(enemyTextPlayer.getBoard().Loser_checker()){
                winner = this.getName();
                break;
            }
            enemyTextPlayer.playOneTurn(this);
            // check if player lost all the ships
            if(this.getBoard().Loser_checker()){
                winner =  enemyTextPlayer.getName();
                break ;
            }
        }
        out.print("---------------------------------------------------------------------------\n");
        out.print("the player " + winner + " win !\n");
        out.print("---------------------------------------------------------------------------\n");
    }

    /**
   * play one turn action
   * 
   * @param enemyTextPlayer the textplay object of ememy
   */ 
  public void playOneTurn(TextPlayer enemyTextPlayer) throws IOException{
    // print out the ocean information if the player is not computer
    if(!IsComputer){
        out.print("---------------------------------------------------------------------------\n");
        out.print("Player " +name+ "'s turn:\n");
        out.print(view.displayMyBoardWithEnemyNextToIt(enemyTextPlayer.getView(), "Your ocean", "Player " + enemyTextPlayer.getName() + "'s ocean"));
    }
    if(action_remain.get('S') == 0 && action_remain.get('M') == 0){
        // if all the S and M oppotunitis are used
        fire_action(enemyTextPlayer);
    }else{
        char input = Computer_choose();
        if(!IsComputer){
            // ask user to input their choose
            while(true){
                // check if the format of action choose is in correct format
                try{
                    input = Character.toUpperCase(DoOneChoose());
                    if(input == 'S' || input == 'M' ||  input == 'F'){
                        break;
                    }
                    throw new IllegalArgumentException("input anction's format is wrong!");
                }catch(IllegalArgumentException e) {
                    out.print(e.getMessage()+"\n");
                }
            }
        }
        // if the player/computer choose the sonar action
        if(input == 'S'){
            // check if there are still action point to use sonar
            if(action_remain.get('S')==0){
                if(!IsComputer){
                    out.print("you can not scan now\n");
                    out.print("---------------------------------------------------------------------------\n");
                }
                // let player choose again
                this.playOneTurn(enemyTextPlayer);
                return;
            }
            // do the sonar action
            while(true){
                try{
                    if(!IsComputer){
                        out.print("---------------------------------------------------------------------------\n");
                        Coordinate check = readCoordinate("where is the center coordinates of a sonar scan?");
                        HashMap<String,Integer> check_result = enemyTextPlayer.theBoard.check_Sonar(check);
                        out.print("---------------------------------------------------------------------------\n");
                        out.print("Submarines occupy " + check_result.get("Submarine") +" squares\n");
                        out.print("Destroyers occupy " + check_result.get("Destroyer") +" squares\n");
                        out.print("Battleships occupy " + check_result.get("Battleship") +" squares\n");
                        out.print("Carriers occupy " + check_result.get("Carrier") +" squares\n");
                        out.print("---------------------------------------------------------------------------\n");
                    }
                    // sonar action remian -1 
                    int Sonar_remain = action_remain.get('S') ;
                    action_remain.replace('S', Sonar_remain-1);
                    // print the computer action
                    if(IsComputer){
                        out.print("---------------------------------------------------------------------------\n");
                        out.print("Player " + this.name + " used a special action\n");
                    }
                    break;
                }catch(IllegalArgumentException e) {
                    if(!IsComputer){
                        out.print(e.getMessage()+"\n");
                    }
                }
            }
        }else if(input == 'F'){
            // do the fire action 
            fire_action(enemyTextPlayer);
        }else{
            // check if the player can move the ship
            if(action_remain.get('M')==0){
                if(!IsComputer){
                    out.print("you can not move ship now\n");
                    out.print("---------------------------------------------------------------------------\n");
                }
                this.playOneTurn(enemyTextPlayer);
                return;
            }
            String ship_Name = "";
            // build a HashSet<Integer> to record the hitted index for the ship
            ArrayList<Integer> hit_indexs = new ArrayList<Integer>();
            try{
                // if the player is computer, then we set the computer will move the first ship
                Ship<Character> ShipToDelete = this.theBoard.getMyShips().get(0);
                // ask the player to input the index to move
                if(!IsComputer){
                    Coordinate delete = readCoordinate("please input the coordinate of ship you want to remove?");
                    out.print("---------------------------------------------------------------------------\n");
                    ShipToDelete = delete_checker(delete);
                }
                // save the name of moved ship
                ship_Name = ShipToDelete.getName();
                // delete the moved ship from board
                this.theBoard.delete_ship(ShipToDelete);
                // record the hitted index for this ship
                for(Coordinate c: ShipToDelete.getCoordinates()){
                    if(ShipToDelete.wasHitAt(c)){
                        hit_indexs.add(ShipToDelete.getIndex(c));
                    }
                }
            }catch(IllegalArgumentException e){
                if(!IsComputer){
                    out.print(e.getMessage()+"\n");
                }
                this.playOneTurn(enemyTextPlayer);
                return;
            }
            // add a new ship to the new coordinate
            addNewShip(ship_Name);
            Ship<Character> Ship = theBoard.getMyShips().get(theBoard.getMyShips().size()-1);
            for(int i = 0 ; i < hit_indexs.size(); i ++){
                // move the hitted message to the new build ship
                Ship.recordHitAt(Ship.getcoordinate_order().get(hit_indexs.get(i)));
            }
            // move action remian -1
            int move_remain =action_remain.get('M') ;
            action_remain.replace('M', move_remain-1);
            out.print("---------------------------------------------------------------------------\n");
            out.print("player "+ this.name + "'s ocean after move is: \n");
            out.print(view.displayMyOwnBoard());
            out.print("---------------------------------------------------------------------------\n");
            if(IsComputer){
                out.print("---------------------------------------------------------------------------\n");
                out.print("Player " + this.name + " used a special action\n");
            }
        }
    }
}
}
