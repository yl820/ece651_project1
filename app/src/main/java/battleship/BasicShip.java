package battleship;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * This interface represents any type of Ship in our Battleship game. It is
 * generic in typename T, which is the type of information the view needs to
 * display this ship.
 */
public abstract class BasicShip<T> implements Ship<T>{
    /*Hashmap of <coordinates in this ship, if the coordinate has been hit>*/
    protected HashMap<Coordinate, Boolean>  myPieces;
    //if myPieces.get(c)  is null, c is not part of this Ship
    //if myPieces.get(c)  is false, c is part of this ship and has not been hit
    //if myPieces.get(c)  is true, c is part of this ship and has been hit

    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    //for us to save the order of creating coordinates
    protected ArrayList<Coordinate> coordinate_order;

    /**
   * get the coordinate order array list
   * 
   * @return the coordinate order array list
   */
    public ArrayList<Coordinate> getcoordinate_order(){
        return coordinate_order;
    }

    /**
   * get the order index of a coordinate in array list
   * 
   * @return order index of a coordinate in array list
   */
    public int getIndex(Coordinate c){
        // check the index of coordinate
        if(this.occupiesCoordinates(c)){
            return coordinate_order.indexOf(c);
        }
        throw new IllegalArgumentException("the format is wrong");
    }

    public BasicShip(Iterable<Coordinate> where,ShipDisplayInfo<T> myDisplayInfo,ShipDisplayInfo<T> enemyDisplayInfo){
        myPieces = new HashMap<Coordinate, Boolean>();
        coordinate_order = new ArrayList<Coordinate>();
        for (Coordinate c : where) {
            myPieces.put(c, false);
            coordinate_order.add(c);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }

    /**
   * Check if this ship occupies the given coordinate.
   * 
   * @param where is the Coordinate to check if this Ship occupies
   * @return true if where is inside this ship, false if not.
   */
    @Override
    public boolean occupiesCoordinates(Coordinate where){
        for (Coordinate this_Coordinate : myPieces.keySet()){
            if(this_Coordinate.equals(where) ){
                return true;
            }
        }
        return false;
    }

    /**
   * Check if this ship has been hit in all of its locations meaning it has been
   * sunk.
   * 
   * @return true if this ship has been sunk, false otherwise.
   */
    @Override
    public boolean isSunk(){
        // for every coordinates in the ship
        for(Boolean infor : myPieces.values()){
            // return true if all coordiantes of this ship have been hit
            if(infor != true){
                return false;
            }else if( infor == null ){
                throw new IllegalArgumentException("there is something wrong with the myPiece function");
            }
        }
        return true;
    }
    
    /**
   * Make this ship record that it has been hit at the given coordinate. The
   * specified coordinate must be part of the ship.
   * 
   * @param where specifies the coordinates that were hit.
   * @throws IllegalArgumentException if where is not part of the Ship
   */
    @Override
    public void recordHitAt(Coordinate where){
        // check if the coordinate if in correct format
        if(!this.occupiesCoordinates(where)){
            throw new IllegalArgumentException("the specified coordinate row: "+ where.getRow() + " column: " + where.getColumn() +" is not a part of the ship.");
        }
        // set the coordiante where to Onhit = true
        myPieces.replace(where, true);
    }
    
    /**
   * Check if this ship was hit at the specified coordinates. The coordinates must
   * be part of this Ship.
   * 
   * @param where is the coordinates to check.
   * @return true if this ship as hit at the indicated coordinates, and false
   *         otherwise.
   * @throws IllegalArgumentException if the coordinates are not part of this
   *                                  ship.
   */
    @Override
    public boolean wasHitAt(Coordinate where){
        // check if the coordinate if in correct format
        if(!this.occupiesCoordinates(where)){
            throw new IllegalArgumentException("the specified coordinate row: "+ where.getRow() + " column: " + where.getColumn() +" is not a part of the ship.");
        }
        // return if the coordinate has been hit
        return myPieces.get(where);
    }
    

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
    //look up the hit status of this coordinate
        if(myShip){
            Boolean temp = myPieces.get(where);
            if(temp == null){
                throw new IllegalArgumentException("the specified coordinate row: "+ where.getRow() + " column: " + where.getColumn() +" is not a part of the ship.");
            }
            // display the info of myShips
            return myDisplayInfo.getInfo(where, temp);
        }else{
            Boolean temp = myPieces.get(where);
            if(temp == null){
                throw new IllegalArgumentException("the specified coordinate row: "+ where.getRow() + " column: " + where.getColumn() +" is not a part of the ship.");
            }
            // display the info of enemyShips
            return enemyDisplayInfo.getInfo(where, temp);
        }
    }

    /**
   * get the coordinate iterable of coordinates of this ship
   * @return the coordinate iterable of coordinates of this ship
   */
    @Override
    public Iterable<Coordinate> getCoordinates(){
        return myPieces.keySet();
    }
}
