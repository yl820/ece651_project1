package battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
   * check if there is a collision on the board 
   * 
   * @param theShip to check 
   * @param theBoard whose theShip has to be checked
   * @return the error description (null if there is no error)
   */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for(Coordinate c : theShip.getCoordinates()){
            if(theBoard.whatIsAt(c,true) != null){
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }
        return null;
    }
    
}
