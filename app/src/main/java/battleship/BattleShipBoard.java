package battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * This class represents the BattleShipBoard in our Battleship game. It is
 * generic in typename T, which is the type of information the view needs to
 * display this ship.
 */
public class BattleShipBoard<T> implements Board<T>  {
    /*width of game's board*/
    private final int width;

    /*height of game's board*/
    private final int height;

    /*the arraylist of ships*/
    final ArrayList<Ship<T>> myShips;

    /*the rulechecker for placement*/
    private final PlacementRuleChecker<T> placementChecker;

    /*the set of miss hit coordinates*/
    HashSet<Coordinate> enemyMisses;

    /*the set of miss information message*/
    final T missInfo;

    /*record the hitted coordinate*/
    HashMap<Coordinate,String> record_hit;

    /*create a map between the ship name and character*/
    HashMap<String,Character> ship_map;

    /**
   * initial the battleShipBoard
   */
    public BattleShipBoard(int w, int h, T missInfo) {
        this(w, h, new NoCollisionRuleChecker<>(new InBoundsRuleChecker<T>(null)),missInfo);
        this.record_hit = new HashMap<Coordinate,String>();
        // create the ship_map
        this.ship_map = new HashMap<String,Character>(){
            {
            put("Submarine", 's');  
            put("Destroyer", 'd');
            put("Battleship", 'b');
            put("Carrier", 'c');
        }
        };
    }
    
    /**
   * @return return the width of the board
   */
    public int getWidth() {
        return this.width;
    }

    /**
   * @return return the height of the board
   */
    public int getHeight() {
        return this.height;
    }

    /**
   * @return return the ship list
   */
    public ArrayList<Ship<T>> getMyShips(){
        return myShips;
    }

    /**
   * return the sonar reuslt HashMap<String,Integer>
   * 
   * @param where the coordinate to start the sonar
   * @return the sonar reuslt HashMap<String,Integer>
   */
    public HashMap<String,Integer> check_Sonar(Coordinate where){
        // record the coordinates in the sonar 
        HashSet<Coordinate> coordinates_set = new HashSet<Coordinate>();
        int row = where.getRow();
        int column = where.getColumn();
        for(int j = -3; j <= 3; j ++){
            int temp = 3 - Math.abs(j);
            for (int iter = column-temp ; iter <= column + temp ; iter ++){
                // check if the point is in the board
                if(row+j>0 && row+j<= this.height && iter > 0 && iter < this.width ){
                    coordinates_set.add(new Coordinate(row+j, iter));
                }
            }
        }
        // intialize the sonar result HashMap<String,Integer>
        HashMap<String,Integer> sonar_result = new HashMap<String,Integer>(){
            {
            put("Submarine", 0);  
            put("Destroyer", 0);
            put("Battleship", 0);
            put("Carrier", 0);
        }
        };
        // record the result into the result Hashmap
        for (Coordinate c : coordinates_set){
            for (Ship<T> ship : myShips ){
                if(ship.occupiesCoordinates(c)){
                    String ship_name = ship.getName();
                    int count = sonar_result.get(ship_name);
                    // 
                    sonar_result.replace(ship_name, count+1);
                }
            }
        }
        return sonar_result;
    }


    public BattleShipBoard(int width,int height,PlacementRuleChecker<T> RuleChecker,T missInfo){
        if(width<=0 || height<=0 || width > 10 || height > 26){
            throw new IllegalArgumentException("the size of the board is unusable");
        }
        this.width = width;
        this.height = height;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = RuleChecker;
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
    }

    /**
   * try to add the ship, return the error message in string
   * ( return null if there is no error)
   * 
   * @param toadd is the ship which is trying to add 
   * @return the error message
   */
    public String tryAddShip(Ship<T> toAdd){
        if(placementChecker.checkPlacement(toAdd, this) == null){
            myShips.add(toAdd);
            return null;
        }
        return placementChecker.checkPlacement(toAdd, this);
    }
    
    /**
   * fire at the coordinate and update the hit ship and miss coordinates
   * 
   * @param c is the coordinate to get fire at
   * @return ship hit at the coordinate c
   */
    public Ship<T> fireAt(Coordinate c){
        if(c.getRow()>height || c.getColumn()<0 || c.getRow()<0 || c.getColumn()>width){
            throw new IllegalArgumentException("That fire is invalid: the coordinate is out of bounds");
        }
        for(Ship<T> this_ship : myShips){
            // find the ship to hit
            if( this_ship.occupiesCoordinates(c)){
                // record hit message
                this_ship.recordHitAt(c);
                // put in into record hit message
                record_hit.put(c,this_ship.getName());
                return this_ship;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    /**
   * get the message at the coordinate
   * 
   * @param where is the coordinate to get the message
   * @param isSelf if it is used to build players message
   * @return message at the coordinate
   */
    @Override
    public T whatIsAt(Coordinate where, boolean isSelf) {
        // check if the coordiante is in correct range
        if(where.getRow()>height || where.getColumn()<0 || where.getRow()<0 || where.getColumn()>width){
            throw new IllegalArgumentException("the coordinate is out of bounds");
        }
        // if it is for player isself 
        if(isSelf){
            for (Ship<T> s: myShips) {
                // find the ship which occupies this coordinate
                if (s.occupiesCoordinates(where) == true){
                    return s.getDisplayInfoAt(where,isSelf);
                }
            }
        }
        // if it is for enemy
        if( isSelf != true ){
            // find the ship which occupies this coordinate
            for (Coordinate c : enemyMisses){
                if(c.equals(where)){
                    return this.missInfo; 
                }
            }
        }
        return null;
    }

    /**
   * get the message at the coordinate for enemy
   * 
   * @param where is the coordinate to get the message
   * @return message at the coordinate
   */
    @SuppressWarnings("unchecked")
    public T whatIsAtForEnemy(Coordinate where) {
        if(record_hit.containsKey(where)){
            // return record hitted message if the coordiante is hitted coordiante
            String ship_name = record_hit.get(where);
            return (T) ship_map.get(ship_name);
        }
        return whatIsAt(where, false);
    }

    /**
    * get the message at the coordinate for player
    * 
    * @param where is the coordinate to get the message
    * @return message at the coordinate
    */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
   * check if the player is loss (all ships are sunk)
   * 
   * @return if the player is loss
   */
    public boolean Loser_checker(){
        for (Ship<T> ship : myShips){
            if(ship.isSunk() != true){
                return false;
            }
        }
        return true;
    }

    /**
    * get the message at the coordinate for player
    * 
    * @param ShipToDelete is the ship we want to delete
    */
    public void delete_ship(Ship<Character> ShipToDelete){
        this.myShips.remove(ShipToDelete);
    }
}
