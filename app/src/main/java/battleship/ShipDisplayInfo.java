package battleship;

/**
 * This interface represents an ship display information for Ship creation.
 */
public interface ShipDisplayInfo<T> {
    public T getInfo(Coordinate where, boolean hit);
}
