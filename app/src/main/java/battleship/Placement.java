package battleship;

public class Placement {
    private final Coordinate where;
    private final char orientation;

    /**
   * get the coordinate where for the placement
   * 
   * @return the coordinate where for the placement
   */
    public Coordinate getWhere() {
        return this.where;
    }

    /**
   * get the Orientation for the placement
   * 
   * @return the Orientation for the placement
   */
    public char getOrientation() {
        return this.orientation;
    }

    /**
   * initialize the placement using (Coordinate where,char orientation)
   * 
   * @param where the coordinate 
   * @param orientation the orientation
   */
    public Placement(Coordinate where,char orientation){
        char upper_orientation = Character.toUpperCase(orientation);
        if(upper_orientation != 'V'&& upper_orientation != 'H'&&upper_orientation !='U'&&upper_orientation !='R'&&upper_orientation !='D'&&upper_orientation !='L'){
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        this.where = where;
        this.orientation = Character.toUpperCase(orientation);
    }

    /**
   * initialize the placement using (String input)
   * 
   * @param input the descr for the placement
   */
    public Placement(String input){
        // check if the length of input is correct
        if(input.length()!=3){
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        String Coordinate_part = input.substring(0, 2);
        char orientation_part = input.charAt(2);; 
        char upper_orientation = Character.toUpperCase(orientation_part);
        // check if the orientation part has the correct format
        if(upper_orientation != 'V' && upper_orientation != 'H'&&upper_orientation !='U'&&upper_orientation !='R'&&upper_orientation !='D'&&upper_orientation !='L'){
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        this.where = new Coordinate(Coordinate_part);
        this.orientation = Character.toUpperCase(orientation_part);
    }

    /**
   * the equal function for placement class
   * 
   * @param descr the string descr for the placement
   */
    @Override
    public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
        Placement c = (Placement) o;
        // check if the coordiante and the orientation is the same at the same time
        return where.equals(c.getWhere()) && orientation == c.getOrientation();
    }
    return false;
    }

    /**
   * display of the placement
   * 
   * @return the display string of the placement
   */
    @Override
    public String toString() {
        int row_index = where.getRow();
        int column_index = where.getColumn();
        char row_char = (char)('A' + row_index);
        return "(" + row_char +"," + column_index + "),"+ orientation ;
    }
    
    /**
   * get hashcode for this placement
   * 
   * @return hashcode for this placement
   */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
