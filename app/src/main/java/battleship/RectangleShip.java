package battleship;

import java.util.ArrayList;

public class RectangleShip<T> extends BasicShip<T>{
    // the name of the ship
    private final String name;

    /**
   * make coordinate in the given location and width, height
   * 
   * @param upperLeft is the upperLeft of the ship
   * @param width of the ship
   * @param height of the ship
   * @return the arraylist of coordinate (record the coordinate and the build order)
   */
    static ArrayList<Coordinate> makeCoords_Rectangle(Coordinate upperLeft, int width, int height){
        ArrayList<Coordinate> ans = new ArrayList<Coordinate>();
        for(int i = upperLeft.getColumn() ; i < upperLeft.getColumn()+ width; i++){
            for(int j = upperLeft.getRow(); j < upperLeft.getRow()+height; j++){     
                Coordinate new_Coordinate = new Coordinate(j,i);
                ans.add(new_Coordinate);
            }
        }
        return ans;
    }

    /**
   * get the name of ship
   * 
   * @return the name of ship
   */
    public String getName(){
        return name;
    }

    /**
   * initialize the RectangleShip
   */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeCoords_Rectangle(upperLeft, width, height), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
   * initialize the RectangleShip
   */
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),new SimpleShipDisplayInfo<T>(null, data));
    }
    
    /**
   * initialize the RectangleShip
   */
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit);
    }
}
