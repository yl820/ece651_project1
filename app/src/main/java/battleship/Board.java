package battleship;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This interface represents any type of board in our Battleship game. It is
 * generic in typename T, which is the type of information the view needs to
 * display this ship.
 */
public interface Board<T> {
    /**
   * @return return the width of the board
   */
    public int getWidth();

    /**
   * @return return the height of the board
   */
    public int getHeight();

    /**
   * get the T at the coordinate
   * 
   * @param where is the coordinate to get the message
   * @param isSelf is the boolean that depend on if it is the board of the player
   * @return message at the coordinate
   */
    public T whatIsAt(Coordinate where, boolean isSelf);

    /**
   * get the message at the coordinate for enemy
   * 
   * @param where is the coordinate to get the message
   * @return message at the coordinate
   */
    public T whatIsAtForEnemy(Coordinate where);

    /**
   * get the message at the coordinate for player
   * 
   * @param where is the coordinate to get the message
   * @return message at the coordinate
   */
    public T whatIsAtForSelf(Coordinate where);

    /**
   * try to add the ship, return the error message in string
   * ( return null if there is no error)
   * 
   * @param toadd is the ship which is trying to add 
   * @return the error message
   */
    public String tryAddShip(Ship<T> toAdd);

    /**
   * fire at the coordinate and update the hit ship and miss coordinates
   * 
   * @param c is the coordinate to get fire at
   * @return ship hit at the coordinate c
   */
    public Ship<T> fireAt(Coordinate c);

    /**
   * check if the player is loss (all ships are sunk)
   * 
   * @return if the player is loss
   */
    public boolean Loser_checker();

    /**
   * return the sonar reuslt HashMap<String,Integer>
   * 
   * @param where the coordinate to start the sonar
   * @return the sonar reuslt HashMap<String,Integer>
   */
    public HashMap<String, Integer> check_Sonar(Coordinate check);

    /**
   * @return return the ship list
   */
    public ArrayList<Ship<T>> getMyShips();

    /**
    * get the message at the coordinate for player
    * 
    * @param ShipToDelete is the ship we want to delete
    */
    public void delete_ship(Ship<Character> shipToDelete);
}
