package battleship;

import java.util.function.Function;

public class BoardTextView {
    private final Board<Character> toDisplay;

    public BoardTextView(Board<Character> theBoard) {
        this.toDisplay = (BattleShipBoard<Character>) theBoard;
        if (theBoard.getWidth() > 10 || theBoard.getHeight() > 26) {
            throw new IllegalArgumentException(
                "Board must be no larger than 10x26, but is " + theBoard.getWidth() + "x" + theBoard.getHeight());
          }
    }
    
    /**
   * this function is used to display the board 
   * 
   * @param getSquareFn is the method we used to know what is at a location
   * @return the board string
   */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn){
        String ans = makeHeader();
        ans = ans + makeBody(getSquareFn) + makeHeader() + '\n';
        return ans.toString();
    }

    /**
   * display the board of mine
   * @return the board string
   */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    /**
   * display the board of enemy
   * @return the board string
   */
    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    /**
   * make the first line of the board
   * @return the head line
   */
    String makeHeader(){
        String ans = "  0";
        String sep = "|"; 
        for (int i = 1; i < toDisplay.getWidth(); i++) {
          ans = ans + sep + Integer.toString(i);
        }
        return ans.toString();
    }

    /**
   * given the way to build the body message of a board
   * 
   * @param getSquareFn is the method we used to know what is at a location
   * @return the body message
   */
    String makeBody(Function<Coordinate, Character> getSquareFn){
        String ans = "\n";
        for (int i = 0; i < toDisplay.getHeight(); i++){
            int temp = (int)'A' + i;
            char letter = (char) temp;
            char sep = '|';
            ans = ans + letter;
            for (int j = 0; j < toDisplay.getWidth(); j++) {
                if (j == 0){
                    ans = ans + ' ';
                }
                Coordinate temp_Coordinate = new Coordinate(i,j);
                Character infor = getSquareFn.apply(temp_Coordinate);
                // if there have some ship then display the information here
                if(infor != null){
                    ans = ans + infor;
                }else{
                    // display space if there are nothing to display
                    ans = ans + " ";
                }
                ans = ans + sep;
                if (j == toDisplay.getWidth()-2 ){
                    sep = ' ';
                }
            }
            ans = ans  + letter + "\n";
        }
        return ans.toString();
    }

    /**
   * display my board with enemy's board nect to mine
   * 
   * @param enemyView is the ship which is trying to add 
   * @param myHeader is the ship which is trying to add 
   * @param enemyHeader is the ship which is trying to add 
   * @return the display message
   */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String [] enemyLines = enemyView.displayEnemyBoard().split("\n");
        // get the list of lines
        String [] Lines = displayMyOwnBoard().split("\n");
        String ans = makeHeaderWithEnemy(myHeader,enemyHeader);
        for(int j = 0; j < Lines.length; j++ ){
            // build the display message lines
            ans = ans + "\n" + Lines[j] + makeSpace(2*toDisplay.getWidth()+19 - Lines[j].length()) + enemyLines[j];
        }
        ans = ans + "\n" ;
        return ans;
    }

    /**
   * make the header line for my board with enemy's board
   * 
   * @param myHeader is the ship which is trying to add 
   * @param enemyHeader is the ship which is trying to add 
   * @return the display message for the header line
   */
    public String makeHeaderWithEnemy(String myHeader, String enemyHeader){
        String ans = makeSpace(5) + myHeader + makeSpace(2*toDisplay.getWidth()+17-myHeader.length()) + enemyHeader;
        return ans;
    }

    /**
   * make the number
   * 
   * @param num the number of space we want to make
   * @return the spefic number of continue spaces
   */
    public String makeSpace(int num){
        String ans = "";
        for (int i = 0; i < num; i ++){
            ans = ans + " ";
        }
        return ans;
    }
}
