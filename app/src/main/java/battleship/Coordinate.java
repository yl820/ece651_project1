package battleship;

public class Coordinate {
    // the row num of coordinate
    private final int row;

    // the column num of coordinate
    private final int column;

    /**
   * get the row number for a coordinate
   * 
   * @return row number for a coordinate
   */
    public int getRow() {
        return this.row;
    }

    /**
   * get the Column number for a coordinate
   * 
   * @return Column number for a coordinate
   */
    public int getColumn() {
        return this.column;
    }

    /**
   * initialize the coordinate with (int row, int column)
   * 
   * @param row the row number of the coordinate
   * @param column the column number of the coordinate
   */
    public Coordinate(int row, int column){
        this.column = column;
        this.row = row;
    }

    /**
   * initialize the coordinate with (String descr)
   * 
   * @param descr the string descr for the coordiante
   */
    public Coordinate(String descr) {
        // check if the descr length is not correct
        if (descr.length() != 2){
            throw new IllegalArgumentException("the length of index for coordinate is wrong, the length of coordinate must be 2");
        }
        String descr_upper =  descr.toUpperCase();
        int row_num = descr_upper.charAt(0) - 'A';
        // check if the first char is letter 
        if ( row_num>26 || row_num<0 ){
            throw new IllegalArgumentException("the row index must be letter");
        }
        int column_num = descr_upper.charAt(1)-'0';
        // check if the second char is digit
        if (  column_num>9 || column_num < 0 ){
            throw new IllegalArgumentException("the cloumn index must be digit, you input is "+ column_num);
        }
        // set the coordinate's row and column
        this.row = row_num;
        this.column = column_num ;
    }

    /**
   * the equal function for coordiante class
   * 
   * @param descr the string descr for the coordiante
   */
    @Override
    public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
        Coordinate c = (Coordinate) o;
        return row == c.row && column == c.column;
        }
        return false;
    }

    /**
   * display of the coordinate
   * 
   * @return the display string of the coordinate
   */
    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    
    /**
   * get hashcode for this coordinate
   * 
   * @return hashcode for this coordinate
   */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}


